#pragma once
#include<sstream>
#include"Date.h"
#include"ParseFromFile.h"
#include "ShowSummerSignOperation.grpc.pb.h"


class ShowSignSummerServiceImpl  final : public ShowSummerSignOperationService::Service
{

public:

	void parseMounthDay(std::string requestDate, short& mounth, short& day);

	class ShowSignSummerServiceImpl() {};

	::grpc::Status ShowTheSign(::grpc::ServerContext* context, const ::ShowSignRequest* request, ::ShowSign* response) override;
};
