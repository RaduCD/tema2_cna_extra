#pragma once
#include<sstream>
#include"Date.h"
#include"ParseFromFile.h"

#include"ShowWinterSignOperation.grpc.pb.h"


#include "ShowSignOperation.grpc.pb.h"

enum class Season
{
	SPRING, 
	SUMMER, 
	AUTUMN,
	WINTER
};

class ShowSignServiceImpl  final : public ShowSignOperationService::Service
{
private:
	Season findTheSeason(short mounth);

public:
	
	void startService();

	void parseMounthDay(std::string requestDate, short& mounth, short& day);

	class ShowSignServiceImpl() {};

	::grpc::Status ShowTheSign(::grpc::ServerContext* context, const ::ShowSignRequest* request, ::ShowSign* response) override;
};


