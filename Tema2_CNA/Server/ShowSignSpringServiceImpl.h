#pragma once
#include<sstream>
#include"Date.h"
#include"ParseFromFile.h"
#include "ShowSpringSignOperation.grpc.pb.h"


class ShowSignSpringServiceImpl  final : public ShowSpringSignOperationService::Service
{

public:

	void parseMounthDay(std::string requestDate, short& mounth, short& day);

	class ShowSignSpringServiceImpl() {};

	::grpc::Status ShowTheSign(::grpc::ServerContext* context, const ::ShowSignRequest* request, ::ShowSign* response) override;
};




