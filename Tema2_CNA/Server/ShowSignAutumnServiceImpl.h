#pragma once
#include<sstream>
#include"Date.h"
#include"ParseFromFile.h"
#include "ShowAutumnSignOperation.grpc.pb.h"


class ShowSignAutumnServiceImpl  final : public ShowAutumnSignOperationService::Service
{

public:

	void parseMounthDay(std::string requestDate, short& mounth, short& day);

	class ShowSignAutumnServiceImpl() {};

	::grpc::Status ShowTheSign(::grpc::ServerContext* context, const ::ShowSignRequest* request, ::ShowSign* response) override;
};
