#include "Sign.h"

Sign::Sign(Date* dateFrom, Date* dateTo, std::string sign)
{
	this->dateFrom = dateFrom;
	this->dateTo = dateTo;
	this->sign = sign;
}

Date* Sign::getDateFrom()
{
	return dateFrom;
}

Date* Sign::getDateTo()
{
	return dateTo;
}

std::string Sign::getSign()
{
	return sign;
}
