#pragma once
#include<sstream>
#include"Date.h"
#include"ParseFromFile.h"
#include "ShowSignOperation.grpc.pb.h"

#include"ShowWinterSignOperation.grpc.pb.h"


class ShowSignWinterServiceImpl  final : public ShowWinterSignOperationService::Service
{

public:

	void parseMounthDay(std::string requestDate, short& mounth, short& day);

	class ShowSignWinterServiceImpl() {};

	::grpc::Status ShowTheSign(::grpc::ServerContext* context, const ::ShowSignRequest* request, ::ShowSign* response) override;
};
