#include "ShowSignSummerServiceImpl.h"

void ShowSignSummerServiceImpl::parseMounthDay(std::string requestDate, short& mounth, short& day)
{
	std::vector <std::string> tokens;
	std::stringstream check(requestDate);
	std::string intermediate;

	while (getline(check, intermediate, '/'))
	{
		tokens.push_back(intermediate);
	}

	std::stringstream toIntegerMounthFormat(tokens[0]);
	toIntegerMounthFormat >> mounth;

	std::stringstream toIntegerDayFormat(tokens[1]);
	toIntegerDayFormat >> day;
}

::grpc::Status ShowSignSummerServiceImpl::ShowTheSign(::grpc::ServerContext* context, const::ShowSignRequest* request, ::ShowSign* response)
{
	std::string signBirthday = request->signbirthday();
	std::cout << "Date is: " << signBirthday << "\n";

	short day, mounth;
	parseMounthDay(signBirthday, mounth, day);
	Date* dateRequest = new Date(mounth, day);

	std::string signResult = ParseFromFile::getInstance()->findTheSignFromDate(dateRequest);


	response->set_sign(signResult);

	return ::grpc::Status::OK;
}
