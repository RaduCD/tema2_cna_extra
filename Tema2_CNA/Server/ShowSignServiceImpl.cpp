#include "ShowSignServiceImpl.h"
#include "ParseFromFile.h"

#include<grpc/grpc.h>
#include<grpcpp/server.h>
#include<grpcpp/server_builder.h>
#include<grpcpp/server_context.h>
#include"ParseFromFile.h"

#include"ShowSignSpringServiceImpl.h"
#include"ShowSignSummerServiceImpl.h"
#include"ShowSignAutumnServiceImpl.h"
#include"ShowSignWinterServiceImpl.h"

#include"ShowWinterSignOperation.grpc.pb.h"
#include"ShowSummerSignOperation.grpc.pb.h"
#include"ShowSpringSignOperation.grpc.pb.h"
#include"ShowAutumnSignOperation.grpc.pb.h"


#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>


using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;


Season ShowSignServiceImpl::findTheSeason(short mounth)
{
	if(mounth == 12 || mounth == 1 || mounth == 2)
		return Season::WINTER;

	if (mounth == 3 || mounth == 4 || mounth == 5)
		return Season::SPRING;

	if (mounth == 6 || mounth == 7 || mounth == 8)
		return Season::SUMMER;

	if (mounth == 9 || mounth == 10 || mounth == 11)
		return Season::AUTUMN;
}

void ShowSignServiceImpl::startService()
{
	std::string server_address("localhost:1235");
	ShowSignServiceImpl service;

	ShowSignSpringServiceImpl springService;
	ShowSignSummerServiceImpl summerService;
	ShowSignAutumnServiceImpl autumnService;
	ShowSignWinterServiceImpl winterService;


	::grpc_impl::ServerBuilder serverBuilder;
	serverBuilder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

	serverBuilder.RegisterService(&springService);
	serverBuilder.RegisterService(&summerService);
	serverBuilder.RegisterService(&autumnService);
	serverBuilder.RegisterService(&winterService);
	std::unique_ptr<::grpc_impl::Server> server(serverBuilder.BuildAndStart());

	//std::vector<Sign*> listSigns = ParseFromFile::getInstance()->initListSign();

	std::cout << "Server listening on " << server_address << std::endl;
	server->Wait();
}

void ShowSignServiceImpl::parseMounthDay(std::string requestDate, short& mounth, short& day)
{
	std::vector <std::string> tokens;
	std::stringstream check(requestDate);
	std::string intermediate;

	while (getline(check, intermediate, '/'))
	{
		tokens.push_back(intermediate);
	}

	std::stringstream toIntegerMounthFormat(tokens[0]);
	toIntegerMounthFormat >> mounth;

	std::stringstream toIntegerDayFormat(tokens[1]);
	toIntegerDayFormat >> day;
}

::grpc::Status ShowSignServiceImpl::ShowTheSign(::grpc::ServerContext* context, const::ShowSignRequest* request, ::ShowSign* response)
{
	std::string signBirthday = request->signbirthday();

	short day, mounth;
	parseMounthDay(signBirthday, mounth, day);
	Date* dateRequest = new Date(mounth, day);


	ClientContext contextClient;

	switch (findTheSeason(mounth))
	{

		case Season::WINTER:
		{
			 auto sum_stub = ShowWinterSignOperationService::NewStub(grpc::CreateChannel("localhost:1235",
			 grpc::InsecureChannelCredentials()));
			 sum_stub->ShowTheSign(&contextClient, *request, response);
			break;
		}

		case Season::SPRING:
		{
			auto sum_stub = ShowSpringSignOperationService::NewStub(grpc::CreateChannel("localhost:1235",
				grpc::InsecureChannelCredentials()));
			sum_stub->ShowTheSign(&contextClient, *request, response);
			break;
		}

		case Season::SUMMER:
		{
			auto sum_stub = ShowSummerSignOperationService::NewStub(grpc::CreateChannel("localhost:1235",
				grpc::InsecureChannelCredentials()));
			sum_stub->ShowTheSign(&contextClient, *request, response);
			break;
		}

		case Season::AUTUMN:
		{
			auto sum_stub = ShowAutumnSignOperationService::NewStub(grpc::CreateChannel("localhost:1235",
				grpc::InsecureChannelCredentials()));
			sum_stub->ShowTheSign(&contextClient, *request, response);
			break;
		}
	}
	
	return ::grpc::Status::OK;
}
